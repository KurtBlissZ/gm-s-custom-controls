con_left    = scc_add("Left"    , scc_type_keyboard, vk_left);
con_right   = scc_add("Right"   , scc_type_keyboard, vk_right);
con_up      = scc_add("Up"      , scc_type_keyboard, vk_up);
con_down    = scc_add("Down"    , scc_type_keyboard, vk_down);
con_a       = scc_add("A"       , scc_type_keyboard, ord("Z"));
con_b       = scc_add("B"       , scc_type_keyboard, ord("X"));
con_x       = scc_add("X"       , scc_type_keyboard, ord("C"));
con_y       = scc_add("Y"       , scc_type_keyboard, ord("V"));

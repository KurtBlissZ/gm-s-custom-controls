///scr_constrols_set(numb,name);
// Int. Control varibles
con_name[argument0] = argument1;
con_type[argument0] = 0; // 0-none, 1-keyboard, 2-con.button, 3-con.axsis, 4-con.pov
con_button[argument0] = 0; // the key, button, axsis, or pov
//con_analog[argument0] = 0; // take advantage of analog, return the ammout pressed
con_controller[argument0] = 0; // witch controller are you using, zero for none
con_ver[argument0] = 0; // 0 is keyboad, 1 is joystick, 2 is gamepad
check_button[argument0] = 0;
check_button_pressed[argument0] = 0;
check_button_released[argument0] = 0;
check_button_previous[argument0] = 0;
check_analog[argument0] = 0;

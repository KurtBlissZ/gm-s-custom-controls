# READ ME #

This project is for Game Maker Studio. Let the player change the controls in game supporting keyboard and gamepads. Also supporting joystick functions for backwards compatibility. This project works on Windows and a little on HTML5. It should be able to work on everything else as well or at least in the future.

Download: https://bitbucket.org/KurtBliss/gm-s-custom-controls/downloads

Download the lagacy version [here](http://gmc.yoyogames.com/index.php?showtopic=548774&hl=) that works on GameMaker 8 and GameMaker Studio. 
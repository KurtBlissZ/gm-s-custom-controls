/// scr_controls_check(control);
var n = argument0;    

if con_type[n] == scc_type_button {
    if gamepad_button_check(con_controller[n],con_button[n]) {
        check_button[n] = true;
        check_analog[n] = gamepad_button_value(con_controller[n],con_button[n]);
    } else { 
        check_button[n] = 0; 
        check_analog[n] = 0;
    }
} else if (con_type[n] == scc_type_axispos) {
    if (gamepad_axis_value(con_controller[n],con_button[n])>axsis_deadzone) {
        check_button[n] = true;
        check_analog[n] = abs(gamepad_axis_value(con_controller[n],con_button[n]));
    } else { 
        check_button[n] = 0; 
        check_analog[n] = 0;
    }
} else if con_type[n] == scc_type_axisneg {
    if (gamepad_axis_value(con_controller[n],con_button[n])<-axsis_deadzone) {
        check_button[n] = true;
        check_analog[n] = abs(gamepad_axis_value(con_controller[n],con_button[n]));
    } else {
        check_button[n] = 0;
        check_analog[n] = 0;
    }
}

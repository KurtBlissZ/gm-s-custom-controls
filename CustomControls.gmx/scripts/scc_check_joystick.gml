/// scr_controls_check_joystick(control)
var n = argument0;

if (con_type[n] == scc_type_button) {
    if joystick_check_button(con_controller[n],con_button[n]) {
        check_button[n]=1; 
        check_analog[n]=1;
    } else {
        check_button[n]=0; 
        check_analog[n]=0;
    }
}

if (con_type[n] == scc_type_axispos) { //axsis, this is like analog buttons or sticks  
    if con_button[n]==1 { //right
        if joystick_xpos(con_controller[n])>axsis_deadzone {
            check_button[n]=1;
            check_analog[n] = abs(joystick_xpos(con_controller[n]));
        }
        else {
            check_button[n]=0;
            check_analog[n]=0;
        }
    }        

    if con_button[n]==3 { //down
        if joystick_ypos(con_controller[n])>axsis_deadzone {
            check_button[n]=1;
            check_analog[n] = abs(joystick_ypos(con_controller[n]));
        } 
        else {
            check_button[n]=0;
            check_analog[n]=0;
        }
    }    
    
    if con_button[n]==5 {
        if joystick_zpos(con_controller[n])>axsis_deadzone {
            check_button[n]=1;
            check_analog[n] = abs(joystick_zpos(con_controller[n]));
        } 
        else {
            check_button[n]=0;
            check_analog[n]=0;
        }
    }    
        
    if con_button[n]==7 {
        if joystick_rpos(con_controller[n])>axsis_deadzone {
            check_button[n]=1;
            check_analog[n] = abs(joystick_rpos(con_controller[n]));
        } else {
            check_button[n]=0;
            check_analog[n]=0;
        }
    }    
    
    if con_button[n]==9 {
        if joystick_upos(con_controller[n])>axsis_deadzone {
            check_button[n]=1;
            check_analog[n] = abs(joystick_upos(con_controller[n]));
        } else {
            check_button[n]=0;
            check_analog[n]=0;
        }
    }    
    
    if con_button[n]==11 {
        if joystick_vpos(con_controller[n])>axsis_deadzone {
            check_button[n]=1;
            check_analog[n] = abs(joystick_vpos(con_controller[n]));
        } else {
            check_button[n]=0;
            check_analog[n]=0;
        }
    }   
} 

if (con_type[n] == scc_type_axisneg) { //axsis, this is like analog buttons or sticks
    if con_button[n]==0 { //left
        if joystick_xpos(con_controller[n])<-axsis_deadzone {
            check_button[n]=1; 
            check_analog[n] = abs(joystick_xpos(con_controller[n]));
        } else {
            check_button[n]=0;
            check_analog[n]=0;
        }
    }       
    if con_button[n]==2 { //up
        if joystick_ypos(con_controller[n])<-axsis_deadzone {
            check_button[n]=1;
            check_analog[n] = abs(joystick_ypos(con_controller[n]));
        } else {
            check_button[n]=0;
            check_analog[n]=0;
        }
    }
    
    if con_button[n]==4 {
        if joystick_zpos(con_controller[n])<-axsis_deadzone {
            check_button[n]=1;
            check_analog[n] = abs(joystick_zpos(con_controller[n]));
        } else {
            check_button[n]=0;
            check_analog[n]=0;
        }
    }

    if con_button[n]==6 {
        if joystick_rpos(con_controller[n])<-axsis_deadzone {
            check_button[n]=1;
            check_analog[n] = abs(joystick_rpos(con_controller[n]));
        } else {
            check_button[n]=0;
            check_analog[n]=0;
        }
    }
      
    if con_button[n]==8 {
        if joystick_upos(con_controller[n])<-axsis_deadzone {
            check_button[n]=1;
            check_analog[n] = abs(joystick_upos(con_controller[n]));
        } else {
            check_button[n]=0;
            check_analog[n]=0;
        }
    }
    
    if con_button[n]==10 {
        if joystick_vpos(con_controller[n])<-axsis_deadzone {
            check_button[n]=1;
            check_analog[n] = abs(joystick_vpos(con_controller[n]));
        } else {
            check_button[n]=0; 
            check_analog[n]=0;
        }
    }
} 

if (con_type[n] == scc_type_joystick_pov) { //Point of view returns 0,45,90,135,180,225,270,or 315
    var pov = joystick_pov(con_controller[n])
    
    if con_button[n]==0 && (pov==0 || pov==45 || pov==315) {
        check_button[n]=1; 
        check_analog[n] = check_button[n];
    } else if con_button[n]==90 && (pov==90 || pov==45 || pov==135) {
        check_button[n]=1; 
        check_analog[n] = check_button[n];
    } else if con_button[n]==180 && (pov==180 || pov==135 || pov==225) {
        check_button[n]=1; 
        check_analog[n] = check_button[n];
    } else if con_button[n]==270 && (pov==270 || pov==225 || pov==315) {
        check_button[n]=1; 
        check_analog[n] = check_button[n];
    } else {
        check_button[n]=0; 
        check_analog[n]=0;
    }
}

/// scr_controls_detect_keyboard(control);
if keyboard_check(vk_escape) return(0);

if keyboard_check_pressed(vk_anykey) {
    con_type[argument0] = scc_type_keyboard;
    con_button[argument0] = keyboard_key;
    con_controller[argument0] = 0;
    check_analog[argument0] = 0;
    return(1);
}

return(0);

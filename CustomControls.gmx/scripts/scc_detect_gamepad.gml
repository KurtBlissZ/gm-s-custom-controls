/// scr_controls_detect_gamepad(control)
if keyboard_check(vk_escape) return(0);

var device;

for (device = 0; (device <= gamepad_get_device_count()) ; device+=1;) {
    if !gamepad_is_connected(device) break;

    for(var i=0;i<=gamepad_button_count(device);i+=1) {
        if gamepad_button_check(device, i) {
            con_type[argument0] = scc_type_button; 
            con_button[argument0] = i;
            con_controller[argument0] = device;
            return(1);
        }
    }
    
    for(var i=0;i<=gamepad_axis_count(device);i+=1) {
        if (gamepad_axis_value(device, i) > 0.5) {
            con_button[argument0] = i;
            con_controller[argument0] = device;
            con_type[argument0] = scc_type_axispos;
            return(1);
        } else if (gamepad_axis_value(device, i) < -0.5) {
            con_button[argument0] = i;
            con_controller[argument0] = device;
            con_type[argument0] = scc_type_axisneg;
            return(1);
        }
    }
}

return(0);
